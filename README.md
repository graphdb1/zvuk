# Getting started

1. Make sure you have your Neo4j Graph DB up and running
2. Install the following Python dependencies:
    ```commandline
    pip install neo4j python-dotenv
    ```
3. Set database connection details in `.env` file
4. Run `main.py`