from neo4j import GraphDatabase, ManagedTransaction


class Database:
    def __init__(self, uri: str, user: str, password: str, database: str | None = None):
        self.driver = GraphDatabase.driver(uri, auth=(user, password), database=database)

    def close(self):
        self.driver.close()

    def fill_data(self):
        with self.driver.session() as session:
            session.execute_write(self._fill_data)

    @staticmethod
    def _fill_data(tx: ManagedTransaction):
        # Add users
        tx.run('CREATE (u:User) SET u.id="ivan.ivanov", u.name = "Ivan Ivanov"')
        tx.run('CREATE (u:User) SET u.id="sam.smith", u.name = "Sam Smith"')
        tx.run('CREATE (u:User) SET u.id="eva.marcel", u.name = "Eva Marcel"')

        # Add artists
        tx.run('CREATE (a:Artist) SET a.id=675068, a.name = "Imagine Dragons"')
        tx.run('CREATE (a:Artist) SET a.id=165131, a.name = "Grigory Leps"')
        tx.run('CREATE (a:Artist) SET a.id=282957, a.name = "Asaf Avidan"')

        # Add tracks
        tx.run('CREATE (t:Track) SET t.id=3703081, t.title = "Demons", t.year=2013')
        tx.run('CREATE (t:Track) SET t.id=42197229, t.title = "Natural", t.year=2018')
        tx.run('CREATE (t:Track) SET t.id=33311009, t.title = "Believer", t.year=2017')

        # Add albums
        tx.run('CREATE (a:Album) SET a.id=1597165, a.title = "Night Visions", a.year=2013')
        tx.run('CREATE (a:Album) SET a.id=6017186, a.title = "Origins", a.year=2018')
        tx.run('CREATE (a:Album) SET a.id=5568718, a.title = "Evolve", a.year=2017')

        # Bind user preferences of artists
        tx.run("""
           MATCH (u:User {id: "ivan.ivanov"})
           MATCH (a:Artist {id: 675068})
           CREATE (u)-[:LIKES]->(a)
        """)
        tx.run("""
           MATCH (u:User {id: "ivan.ivanov"})
           MATCH (a:Artist {id: 165131})
           CREATE (u)-[:LIKES]->(a)
        """)
        tx.run("""
           MATCH (u:User {id: "sam.smith"})
           MATCH (a:Artist {id: 675068})
           CREATE (u)-[:LIKES]->(a)
        """)
        tx.run("""
           MATCH (u:User {id: "sam.smith"})
           MATCH (a:Artist {id: 282957})
           CREATE (u)-[:DISLIKES]->(a)
        """)

        # Bind tracks to artists
        tx.run("""
            MATCH (t:Track {id: 3703081})
            MATCH (a:Artist {id: 675068})
            CREATE (t)-[:BELONGS_TO]->(a)
        """)
        tx.run("""
            MATCH (t:Track {id: 42197229})
            MATCH (a:Artist {id: 675068})
            CREATE (t)-[:BELONGS_TO]->(a)
        """)
        tx.run("""
            MATCH (t:Track {id: 33311009})
            MATCH (a:Artist {id: 675068})
            CREATE (t)-[:BELONGS_TO]->(a)
        """)

        # Bind tracks to albums
        tx.run("""
           MATCH (t:Track {id: 3703081})
           MATCH (a:Album {id: 1597165})
           CREATE (t)-[:BELONGS_TO]->(a)
        """)
        tx.run("""
             MATCH (t:Track {id: 42197229})
             MATCH (a:Album {id: 6017186})
             CREATE (t)-[:BELONGS_TO]->(a)
          """)
        tx.run("""
             MATCH (t:Track {id: 33311009})
             MATCH (a:Album {id: 5568718})
             CREATE (t)-[:BELONGS_TO]->(a)
        """)

        # Create a playlist, add it to user's collection and add some tracks to it
        tx.run('CREATE (p:Playlist) SET p.id=6840, p.title = "Some music"')
        tx.run("""
            MATCH (u:User {id: "eva.marcel"})
            MATCH (p:Playlist {id: 6840})
            CREATE (p)-[:BELONGS_TO]->(u)
        """)
        tx.run("""
          MATCH (t:Track {id: 3703081})
          MATCH (p:Playlist {id: 6840})
          CREATE (t)-[:BELONGS_TO]->(p)
        """)
        tx.run("""
             MATCH (t:Track {id: 33311009})
             MATCH (p:Playlist {id: 6840})
             CREATE (t)-[:BELONGS_TO]->(p)
        """)

        # Add genres, bind tracks and albums to these genres
        tx.run('CREATE (g:Genre) SET g.id="worldwide rock", g.title = "worldwide rock"')
        tx.run('CREATE (g:Genre) SET g.id="alternative", g.title = "alternative"')
        tx.run("""
             MATCH (g:Genre {id: "worldwide rock"})
             MATCH (a:Album {id: 5568718})
             CREATE (a)-[:BELONGS_TO]->(g)
        """)
        tx.run("""
             MATCH (g:Genre {id: "worldwide rock"})
             MATCH (a:Album {id: 6017186})
             CREATE (a)-[:BELONGS_TO]->(g)
        """)
        tx.run("""
            MATCH (g:Genre {id: "alternative"})
            MATCH (a:Album {id: 6017186})
            CREATE (a)-[:BELONGS_TO]->(g)
        """)
        tx.run("""
           MATCH (g:Genre {id: "alternative"})
           MATCH (t:Track {id: 3703081})
           CREATE (t)-[:BELONGS_TO]->(g)
        """)
        tx.run("""
           MATCH (g:Genre {id: "worldwide rock"})
           MATCH (t:Track {id: 42197229})
           CREATE (t)-[:BELONGS_TO]->(g)
        """)
        tx.run("""
          MATCH (g:Genre {id: "worldwide rock"})
          MATCH (t:Track {id: 33311009})
          CREATE (t)-[:BELONGS_TO]->(g)
       """)
