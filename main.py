import os
from dotenv import load_dotenv
from database import Database


if __name__ == '__main__':
    load_dotenv()

    db = Database(
        os.getenv("DB_URI"),
        os.getenv("DB_USERNAME"),
        os.getenv("DB_PASSWORD"),
        os.getenv("DB_NAME")
    )

    db.fill_data()
    db.close()
